var mongoose = require('mongoose');
var router = require('express').Router();

var auth = require('../../helpers/auth');

var Table = mongoose.model('Table');
var Restaurant = mongoose.model('Restaurant');
var RestaurantOwner = mongoose.model('RestaurantOwner');


router.post('/:restaurantId', auth.required, function(req, res, next) {
	
	var regExObjectId = /^[a-f\d]{24}$/i;
	if (!regExObjectId.test(req.params.restaurantId)) return next();

	RestaurantOwner.findById(req.user.id).then(function(restaurantOwner) {
		if (!restaurantOwner) return res.sendStatus(401);

		if (!req.body.tables || !(req.body.tables instanceof Array))
			return res.sendStatus(400);

		Restaurant.findOne({
			admin: req.user.id,
			_id: req.params.restaurantId
		}).then(function(restaurant) {
			if (!restaurant) return res.sendStatus(401);

			
			let tables = [];

			
			req.body.tables.forEach(function(table) {
				if (!(table instanceof Object)) {
					var err = new Error('Invalid input');
					err.name = 'ValidationError';
					throw err;
				}

				let data = {
					restaurant: req.params.restaurantId,
					tableIdentifier: table.tableIdentifier,
					capacity: table.capacity,
					description: table.description,
					fee: table.fee
				};

				tables.push(data);
			});

			Table.insertMany(tables).then(function(tables) {
				let list = [];

				tables.forEach(function(table) {
					list.push(table.viewJSON());
				});

				return res.json({tables: list});
			}).catch(next);
		}).catch(next);
	}).catch(next);
});




router.get('/:restaurantId/:tableId', auth.required, function(req, res, next) {
	
	Restaurant.findOne({
		admin: req.user.id,
		_id: req.params.restaurantId
	}).then(function(restaurant) {
		if (!restaurant) return res.sendStatus(401);

		Table.findById(req.params.tableId).then(function(table) {
			return res.json({table: table.viewJSON()});
		}).catch(next);
	}).catch(next)
});

module.exports = router;
