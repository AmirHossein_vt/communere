var router = require('express').Router();


router.use('/', require('./restaurant'));
router.use('/table', require('./table'));
router.use('/tables', require('./tables'));
router.use('/booking', require('./booking'));



module.exports = router;
