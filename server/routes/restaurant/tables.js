var mongoose = require('mongoose');
var router = require('express').Router();

var auth = require('../../helpers/auth');
var listTables = require('../../helpers/table/listTables');

var Table = mongoose.model('Table');
var Restaurant = mongoose.model('Restaurant');


router.get('/:restaurantId', auth.required, function(req, res, next) {
	Restaurant.findOne({
		admin: req.user.id,
		_id: req.params.restaurantId
	}).then(function(restaurant) {
		if (!restaurant) return res.sendStatus(401);

		let availability = (typeof(req.query.availability) == 'string'
				&& ['available', 'unavailable', 'status'].indexOf(req.query.availability) != -1)
			? req.query.availability : false;

		let query = {}
		query.bookingId = req.query.bookingId;
		query.date = parseInt(req.query.date) ? parseInt(req.query.date) : (Date.now());
		query.capacity = parseInt(req.query.capacity) ? parseInt(req.query.capacity) : null;
		query.maxcapacity = parseInt(req.query.maxcapacity) ? parseInt(req.query.maxcapacity) : null;
		query.mincapacity = parseInt(req.query.mincapacity) ? parseInt(req.query.mincapacity) : 0;

		if (!availability) {
			listTables('all', req.params.restaurantId, query, next)
			.then(function(list) {
				return res.json({tables: list});
			}).catch(next);
		} else {
			listTables(availability, req.params.restaurantId, query, next)
			.then(function(list) {
				return res.json({tables: list});
			}).catch(next);
		}
	}).catch(next)
});

module.exports = router;
