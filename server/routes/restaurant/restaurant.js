var mongoose = require('mongoose');
var router = require('express').Router();

var auth = require('../../helpers/auth');

var Restaurant = mongoose.model('Restaurant');
var RestaurantOwner = mongoose.model('RestaurantOwner');


router.post('/', auth.required, function(req, res, next) {
	RestaurantOwner.findById(req.user.id).then(function(restaurantOwner) {
		if (!restaurantOwner) return res.sendStatus(401);

		if (!req.body.restaurant) return res.sendStatus(400);

		var restaurant = new Restaurant();

		restaurant.admin = req.user.id;
		restaurant.name = req.body.restaurant.name;
		restaurant.location = req.body.restaurant.location;
		restaurant.address = req.body.restaurant.address;
		restaurant.description = req.body.restaurant.description;
		restaurant.setBusinessHours(req.body.restaurant.businessHours);

		restaurant.save().then(function() {
			return res.json({restaurant: restaurant.viewByOwnerJSON()});
		}).catch(next);
	}).catch(next);
});


router.get('/', auth.required, function(req, res, next) {
	RestaurantOwner.findById(req.user.id).then(function(restaurantOwner) {
		if (!restaurantOwner) return res.sendStatus(401);

		Restaurant.find({admin: req.user.id}).then(function(restaurants) {
			var restauratnsDetails = [];
			restaurants.forEach(function(restaurant) {
				restauratnsDetails.push(restaurant.viewByOwnerJSON());
			});

			return res.json({restaurants: restauratnsDetails});
		}).catch(next);
	}).catch(next);
});


module.exports = router;
