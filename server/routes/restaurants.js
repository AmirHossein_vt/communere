var mongoose = require('mongoose');
var router = require('express').Router();

var Restaurant = mongoose.model('Restaurant');


router.get('/', function(req, res, next) {

	if (req.query.restaurantid) query._id = req.query.restaurantid;

	Restaurant.find().then(function(restaurants) {
		if (!restaurants.length) return res.sendStatus(404);

		let list = []

		restaurants.forEach(function(restaurant) {
			list.push(restaurant.viewJSON());
		});

		return res.json({restaurants: list});
	}).catch(next);
});

module.exports = router;


