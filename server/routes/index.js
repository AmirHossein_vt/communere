var router = require('express').Router();

router.get('/ping', function(req, res, next){
    return res.status(200).send('Hello');
});


router.use('/user', require('./user'));
router.use('/restaurant', require('./restaurant/'));
router.use('/restaurants', require('./restaurants'));
router.use('/booking', require('./booking'));

module.exports = router;
