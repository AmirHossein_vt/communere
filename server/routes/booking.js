var mongoose = require('mongoose');
var router = require('express').Router();

var auth = require('../helpers/auth');
var config = require('../config');
var throwError = require('../helpers/throwError');
var bookingValidator = require('../helpers/bookingValidator');

var Booking = mongoose.model('Booking');
var Table = mongoose.model('Table');
var Customer = mongoose.model('Customer');
var RestaurantOwner = mongoose.model('RestaurantOwner');
var Restaurant = mongoose.model('Restaurant');


router.post('/', auth.required, function(req, res, next) {
	Customer.findById(req.user.id).then(function(customer) {
		if (!customer) return res.sendStatus(401);

		if (!req.body.booking) return res.sendStatus(400);
		var payload = req.body.booking;

		var regExObjectId = /^[a-f\d]{24}$/i;
		if (!regExObjectId.test(payload.restaurant))
			throwError.validationError();
		if (!(payload.noOfPersons = parseInt(payload.noOfPersons))
			|| payload.noOfPersons <= 0)
			throwError.validationError();
		if (payload.bookingFrom < Date.now())
			throwError.validationError();

		
		Restaurant.findById(payload.restaurant)
		.then(function(restaurant) {
			if (!restaurant )
				throwError.validationError('Invalid restaurant');

			
			bookingValidator.businessHours(payload).then(function(valid) {
				if (!valid) throwError.validationError('Restaurant will be closed at that time');

				
				Booking.find(
					{
						'restaurant': payload.restaurant,
						'bookingFrom': {
							$gt: payload.bookingFrom - config.defaultBookingDuration,
							$lt: payload.bookingFrom + config.defaultBookingDuration
						}
					},
					'tables'
				).then(function(occupiedTables) {
					console.log('Occupied tables: ', occupiedTables);

					
					Table.find(
						{
							'restaurant': payload.restaurant,
							'capacity': {$gte: payload.noOfPersons}
						},
						'_id capacity'
					).then(function(allTables) {
						console.log('All tables: ', allTables);
						if (!allTables.length) throwError.noTable('Table not available');

						
						
						let availableTables = allTables.filter(x =>
							!(JSON.parse(JSON.stringify(occupiedTables.map(a => a.tables[0])))
							.includes(JSON.parse(JSON.stringify(x._id)))));

						if (!availableTables.length) throwError.noTable('Table not available');
						console.log('Available tables', availableTables);

						
						let len = availableTables.length;
						let min = Infinity;
						let table = '';

						while(len-- && (min != payload.noOfPersons)) {
							if (availableTables[len].capacity < min) {
								min = availableTables[len].capacity;
								table = availableTables[len]._id;
							}
						}
						console.log('Selected table: ', table)

						
						var booking = new Booking;

						booking.customer = req.user.id;
						booking.restaurant = payload.restaurant;
						booking.noOfPersons = payload.noOfPersons;
						booking.bookingFrom = payload.bookingFrom;
						booking.tables = table;

						booking.save()
						.then(function() {
							return res.json({booking: booking.toCustomerJSON()});
						}).catch(next);
					}).catch(next);
				}).catch(next);
			}).catch(next);
		}).catch(next);
	}).catch(next);
});

/*
 * Get all bookings of the logged-in customer
 * Required data: Authentication token for customer
 */
router.get('/', auth.required, function(req, res, next) {
	Customer.findById(req.user.id).then(function(customer) {
		if (!customer) return res.sendStatus(401);

		
		Booking.find({customer: req.user.id})
		.populate('restaurant', 'name')
		.then(function(bookings) {
			var bookingList = [];
			bookings.forEach(function(booking) {
				bookingList.push(booking.toCustomerJSON());
			});

			res.json({bookings: bookingList});
		}).catch(next);
	}).catch(next);
});

/*
 * Get booking of the logged-in customer by bookingId
 * Required data: Authentication token
 */
router.get('/:bookingId', auth.required, function(req, res, next) {
	
	var regExObjectId = /^[a-f\d]{24}$/i;
	if (!regExObjectId.test(req.params.bookingId)) return next();

	Customer.findById(req.user.id).then(function(customer) {
		if (!customer) return res.sendStatus(401);

		
		Booking.findById({
			_id: req.params.bookingId,
			customer: req.user.id
		}).then(function(booking) {
			if (!booking) return res.sendStatus(401);

			res.json({booking: booking.toCustomerJSON()});
		}).catch(next);
	}).catch(next);
});

module.exports = router;
