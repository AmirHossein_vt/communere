var mongoose = require('mongoose');

var Table = mongoose.model('Table');
var Booking = mongoose.model('Booking');
var Restaurant = mongoose.model('Restaurant');

var config = require('../../config');
var utility = require('../utility');

//
var checkAvailability = function(tableId, restaurant, date, next, bookingId) {
	console.log('Checking the availability of table ', tableId);
	return new Promise(async function(resolve,reject){
		
		await Restaurant.findById(restaurant, 'businessHours')
		.then(function(data) {
			let businessHours = (data.businessHours[utility.time.getDay(date)]);
			let time = utility.time.get(date);

			if (time < businessHours.start || time > businessHours.end) {
				
				console.log('Restaurant close');
				resolve(false);
			} else {
				
				
				Booking.find({
					restaurant: restaurant,
					_id: {$ne: bookingId},
					bookingFrom: {
						$gt: date - config.defaultBookingDuration,
						$lt: date + config.defaultBookingDuration
					}
				}, 'tables')
				.then(function(bookings) {
					if (bookings.map(x => JSON.parse(JSON.stringify(x.tables[0]))).includes(tableId))
						resolve(false);
					else
						resolve(true);
				}).catch(next);

			}
		}).catch(next);
	});
}

module.exports = checkAvailability;
