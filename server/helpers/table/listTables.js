var mongoose = require('mongoose');

var Table = mongoose.model('Table');
var Booking = mongoose.model('Booking');
var Restaurant = mongoose.model('Restaurant');

var config = require('../../config');
var utility = require('../utility');


var listTables = function(availability, restaurant, query, next) {
	return new Promise(async function(resolve,reject){
		
		if (availability == 'all') {
			resolve(await listAllTables(restaurant, query))	
		}

		let restaurantOpen = undefined;

		
		if (availability != 'all') {
			
			await Restaurant.findById(restaurant, 'businessHours')
			.then(function(data) {
				let businessHours = (data.businessHours[utility.time.getDay(query.date)]);
				let time = utility.time.get(query.date);

				if (time < businessHours.start || time > businessHours.end) {
					restaurantOpen = false;
				} else {
					restaurantOpen = true;
				}
			}).catch(next);
		}
		console.log('restaurantOpen = ', restaurantOpen);

		
		if (restaurantOpen)	{	
			
			let dbQuery = {};
			dbQuery.restaurant = restaurant;

			
			dbQuery.bookingFrom = {
				$gt: query.date - config.defaultBookingDuration,
				$lt: query.date + config.defaultBookingDuration
			}

			
			dbQuery._id = {$ne: query.bookingId}

			Booking.find(dbQuery, 'tables')
			.populate('tables')
			.then(async function(bookings) {

				
				if (availability == 'unavailable') {

					
					let list = [];
					bookings.forEach(function(booking) {
						list.push(booking.tables[0].viewJSON());
					});

					resolve(list);

				} else if (availability == 'available' || availability == 'status') {

					let allTables = await listAllTables(restaurant, query);

					let occupiedTables = bookings.map(a => a.tables[0].id);

					
					if (availability == 'status') {
						let allTablesWithStatus = allTables;
						allTablesWithStatus.map(x => {
							if ((occupiedTables).includes(JSON.parse(JSON.stringify(x.id)))) {
								x.availability = 'unavailable';
								return x;
							}
							else {
								x.availability = 'available';
								return x;
							}
						});
						
						resolve(allTablesWithStatus);
					} else {
						

						
						let availableTables = allTables.filter(x =>
							!(occupiedTables).includes(JSON.parse(JSON.stringify(x.id)))
						);

						resolve(availableTables);
					}
				}
			}).catch(next);
		}

		
		if (!restaurantOpen)	{	
			if (availability == 'unavailable') {
				
				resolve(await listAllTables(restaurant, query));
			} else if (availability == 'available') {
				
				resolve([]);
			} else if (availability == 'status') {
				
				let allTables = await listAllTables(restaurant, query);
				allTables = allTables.map(x => {
					x.availability = 'unavailable';
					return x;
				});
				resolve(allTables);
			}
		}
	});
};


var listAllTables = function(restaurant, query) {
	let dbQuery = {};
	dbQuery.restaurant = restaurant;

	
	if (query.capacity) {
		dbQuery.capacity = query.capacity;
	} else if (query.mincapacity || query.maxcapacity) {
		dbQuery.capacity = {};

		if (query.mincapacity) {
			dbQuery.capacity.$gte = query.mincapacity;
		}
		if (query.maxcapacity) {
			dbQuery.capacity.$lte = query.maxcapacity;
		}
	}
	console.log(dbQuery);

	return new Promise(function(resolve,reject){
		Table.find(dbQuery)
		.then(function(tables) {
			let list = [];
			tables.forEach(function(table) {
				list.push(table.viewJSON());
			});
			resolve(list);
		}).catch(e => reject(e));
	});
}

module.exports = listTables;
