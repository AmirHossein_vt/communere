var mongoose = require('mongoose');
var config = require('./../../config');


module.exports = (app) => {
    mongoose.connect(config.mongodb.URL, config.mongodb.option)
        .then(function () {
            console.log('\x1b[32m%s\x1b[0m', 'Database Connection Established!');
            app.emit('databaseReady');
        })
        .catch(function (err) {
            console.log('\x1b[31m%s\x1b[0m', 'Error in Database Connection!');
            console.log(err);
            process.exit(1)
        });
}