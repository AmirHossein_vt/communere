var mongoose = require('mongoose');
var config = require('../../config');

var RestaurantOwner = require('../../models/RestaurantOwner');



const injectSeeds = () => {
	mongoose.connect(config.mongodb.URL, config.mongodb.option)
		.then(function () {
			var user = new RestaurantOwner;

			user.firstName = 'Super';
			user.lastName = 'Admin';
			user.email = 'admin@admin.com';
			user.phone = '1234567890';
			user.setPassword('password');

			user.save().then(function () {
				console.log("administrator information : \r\n" + JSON.stringify(user.toAuthJSON()));

			}).catch((err) => {
				console.log('\x1b[33m%s\x1b[0m', err.toString().includes("already") ? "administrator of system already created" : err);

			});
		})
		.catch(function (err) {
			console.log('\x1b[31m%s\x1b[0m', 'Error in Database Connection!');
			console.log(err);
		});

};


module.exports = injectSeeds