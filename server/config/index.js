var environments = {};

// Staging environments
environments.staging = {
	'port' : 3000,
	'envName' : 'staging',
	'secret' : 'ThisIsSecretCodeForJWT',
	'mongodb' : {
		'URL' : 'mongodb://localhost:27017/',
		'option' : {
			'dbName' : 'Booking',
			'useNewUrlParser' : true
		}
	},
	'defaultBookingDuration': 0100 
};

// Production environments
environments.production = {
	'port' : 5000,
	'envName' : 'production',
	'secret' : process.env.JWT_SECRET || 'ThisIsSecretCodeForJWT',
	'mongodb' : {
		'URL' : process.env.MONGO_URL || 'mongodb://localhost:27017/',
		'option' : {
			'dbName' : 'RTBS',
			'useNewUrlParser' : true
		}
	},
	'defaultBookingDuration': 0100 
};

var currentEnvironment =
	typeof(process.env.NODE_ENV) == 'string'
	? process.env.NODE_ENV.toLowerCase()
	: '';

var environmentToExport =
	typeof(environments[currentEnvironment]) == 'object'
	? environments[currentEnvironment]
	: environments.staging;

module.exports = environmentToExport;
