import React from 'react';
import type { Node } from 'react';
import { Provider } from 'react-redux';
import store from './src/stores';
import { RenderNavigationContainer, SnackBarService } from './src/services';



const App: () => Node = () => {
  return (
    <Provider store={store}>
      <RenderNavigationContainer>
        <SnackBarService />
      </RenderNavigationContainer>
    </Provider>

  );
};


export default App;
