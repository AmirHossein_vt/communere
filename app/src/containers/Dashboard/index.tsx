import React from 'react';
import { View } from 'react-native';
import { Map } from "./../../components/common/Map"

export const DashboardScreen = () => {



  return (
    <View style={{
      height: "100%",
    }}>

      <Map
        addresses={[
          {
            lng: 52.134275, lat: 35.042365
          },
          {
            lng: 51.760495, lat: 35.222063
          },
        ]}
      />

    </View>
  );
};
