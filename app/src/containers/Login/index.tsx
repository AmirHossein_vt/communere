import React , {useState,useReducer} from 'react';
import { View } from 'react-native';
import { navigate } from '../../services/navigationService';
import { showToast } from '../../services/snackBarService';
import { Text, Input, Button } from '../../components/common';
import { loginRequest } from '../../helpers/endpoints';
import { dialogues , Color } from '../../constants';
import { userReducer , initialState } from './../../stores/user/resucers'
import { NEW_USER } from '../../stores/user/types';
import { UserModel } from '../../models/user';


export const LoginScreen = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [_, dispatch] = useReducer(userReducer, initialState)

  const submitLogin = async  ()=>{
    const {user} = await loginRequest({email,password});
    showToast({
      text : dialogues.ok,
      mode : 'success'
    })
    UserModel.setter(user,dispatch)
    navigate("Tab",{
      screenName:"DashboardScreen"
    })
  };



  return (
    <View style={{
      height: "100%",
      justifyContent: "center",
      alignItems: "center"

    }}>

      <Input onChangeText={setEmail} placeHolder="Email" />
      <Input onChangeText={setPassword} placeHolder="Password" />
      <Button mainColor={Color.green} disabled={!email || !password} onPress={submitLogin} title="Login" />


    </View>
  );
};

