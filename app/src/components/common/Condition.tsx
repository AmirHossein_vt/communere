import React from "react";
import {View} from "react-native";

export const Condition = ({children, condition , elseAction = () => <></> , hidden = false}) => {

    if (condition && !hidden) {
        return children
    } else if (!hidden) {
        return elseAction()
    }


    return(
        <View style={{display: condition ? "flex" : "none"}} >
            {children}
        </View>
    );
};
