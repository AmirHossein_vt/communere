import React, { useState } from 'react';
import {
  TouchableNativeFeedback,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Platform,
  View,
  Keyboard,
} from 'react-native';
import PropTypes from 'prop-types';
import VibrationService from '../../services/vibrationService';

const Touch = ({
  children = <></>,
  type,
  onPress,
  onLongPress = () => {},
  disabled,
  hasVibration = false,
  vibrateDuration = 30,
  activeOpacity = 0.85,
  hasShadow,
  onDoublePress = null,
  ...props
}) => {
  let ButtonComponent = null;
  let customProps = {};

  const [clickCount, setClickCount] = useState(0);
  const onUserPress = (long = false) => {
    if (onDoublePress) {
      detectPress();
    } else {
      if (long) {
        onLongPress();
      } else {
        onPress();
      }
      hasVibration && VibrationService.tiny(vibrateDuration);
    }
  };

  const onButtonPress = () => {
    Keyboard.dismiss();
    onPress();
  };

  const detectPress = () => {
    if (onDoublePress) {
      if (clickCount === 0) {
        setClickCount(1);
        setTimeout(() => {
          setClickCount(0);
        }, 500);
      } else {
        setClickCount(0);
        onDoublePress();
      }
    } else {
      onButtonPress();
    }
  };

  switch (type) {
    case 'opacity':
      ButtonComponent = TouchableOpacity;
      customProps = {
        activeOpacity,
      };
      break;
    case 'native':
      if (Platform.OS === 'android') {
        ButtonComponent = TouchableNativeFeedback;
        customProps = {
          background: TouchableNativeFeedback.SelectableBackground(),
        };
      } else {
        ButtonComponent = TouchableWithoutFeedback;
      }
      break;
    case 'highlight':
      ButtonComponent = TouchableHighlight;
      break;
    default:
    case 'without':
      ButtonComponent = TouchableWithoutFeedback;
      break;
  }

  return (
    <ButtonComponent
      onLongPress={onLongPress}
      disabled={disabled}
      onPress={() => !disabled && onUserPress(false)}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...{ ...customProps, ...props }}
    >
      <View>{children}</View>
    </ButtonComponent>
  );
};

Touch.propTypes = {
  // eslint-disable-next-line react/require-default-props
  type: PropTypes.oneOf([
    'opacity',
    'native',
    'highlight',
    'without',
  ]),
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
};

Touch.defaultProps = {
  disabled: false,
  onPress: () => false,
};

export { Touch };
