import React, { useEffect, useState } from 'react';
import { TextInput } from 'react-native';
import { Color } from '../../constants';


export const Input = ({
  placeHolder = '',
  styles = {},
  keyboardType = 'default',
  makePlaceHolderCenter = false,
  placeholderTextColor = Color.lightGray,
  maximumLenght = 1000,
  returnKeyType = 'done',
  injectValue = null,
  onKeyUp,
  numberOfLines = 1,
  editable = true,
  onChangeText = () => {},
  onSubmitEditing = null,
  onFocus = null,
  onBlur = null,
}) => {
  const [value, setValue] = useState('');
  const [isTyping, setIsTyping] = useState({
    name: '',
    typing: false,
    typingTimeout: 0,
  });

  useEffect(() => {
    if (onKeyUp) {
      if (isTyping.typingTimeout) {
        clearTimeout(isTyping.typingTimeout);
      }
      setIsTyping({
        name: value,
        typing: false,
        typingTimeout: setTimeout(() => {
          onKeyUp(value);
        }, 500),
      });
    } else {
      onChangeText(value);
    }
  }, [value]);

  useEffect(() => {
    if (injectValue !== null) {
      setValue(injectValue);
    }
  }, [injectValue]);


  return (
      <TextInput
        onFocus={onFocus && onFocus}
        onBlur={onBlur && onBlur}
        onSubmitEditing={onSubmitEditing && onSubmitEditing}
        numberOfLines={numberOfLines}
        value={value}
        editable={editable}
        returnKeyType={returnKeyType}
        maxLength={maximumLenght}
        onChangeText={setValue}
        keyboardType={keyboardType}
        placeholderTextColor={placeholderTextColor}
        placeholder={placeHolder}
        style={[
          {
            width: '90%',
            fontFamily: 'Shabnam',
            borderBottomWidth: 0.5,
            marginVertical:10
          },
          styles && styles,
          makePlaceHolderCenter && { textAlign: 'center' },
        ]}
      />
  );
};

