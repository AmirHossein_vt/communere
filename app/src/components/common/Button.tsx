import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  Keyboard,
  ActivityIndicator,
  Animated,
} from 'react-native';
import PropTypes from 'prop-types';
import { Touch } from './Touch';
import { Text } from './Text';
import { Color } from '../../constants/theme';
import { Condition } from './Condition';

const Button = 
  ({
    disabled,
    loading,
    shadowColor,
    onPress,
    style,
    type,
    title,
    width,
    height,
    mainColor,
    titleType,
    radius = 10,
    textStyle = {},
    customStyle = {},
    boldText = false,
    onDoublePress = null,
    numberOfTitleLine = null,
  }) => {
    const [clickCount, setClickCount] = useState(0);
    const animation = new Animated.Value(0);
    const inputRange = [0, 1];
    const outputRange = [1, 0.91];
    const scale = animation.interpolate({ inputRange, outputRange });

    const onPressIn = () => {
      Animated.spring(animation, {
        toValue: 1,
        useNativeDriver: true,
      }).start();
    };
    const onPressOut = () => {
      Animated.spring(animation, {
        toValue: 0,
        useNativeDriver: true,
      }).start();
    };

    const color = mainColor || Color.lightGray;



    const onButtonPress = () => {
      Keyboard.dismiss();
      onPress();
    };

    const detectPress = () => {
      if (onDoublePress) {
        if (clickCount === 0) {
          setClickCount(1);
          setTimeout(() => {
            setClickCount(0);
          }, 500);
        } else {
          setClickCount(0);
          onDoublePress();
        }
      } else {
        onButtonPress();
      }
    };

    return (
      <Touch
        disabled={loading || disabled}
        loading={loading}
        onPress={detectPress}
        onLongPress={onDoublePress}
        type="opacity"
        hasShadow={type === 'mini'}
        onPressIn={onPressIn}
        onPressOut={onPressOut}
        style={[customStyle]}
      >

        <Animated.View
          style={[
            {
              borderRadius: radius,
              height,
              width,
              backgroundColor: color,
              shadowColor: shadowColor || color,
              shadowOffset: {
                width: 0,
                height: 4,
              },
              shadowOpacity: 0.3,
              shadowRadius: 4.65,
              opacity: disabled || loading ? 0.5 : 1,
              marginVertical:20
            },
            style && style,
            { transform: [{ scale }] },
          ]}
        >
          <View style={[s.center, s.primaryStyle]}>
            <Condition condition={loading}>
              <ActivityIndicator color={Color.light} size="small" />
            </Condition>
            <View
              style={{
                flexDirection: 'row-reverse',
                display: loading ? 'none' : 'flex',
                width: '100%',
              }}
            >
          
              {title && (
                <View
                  style={[
                    s.title,
                    {
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    },
                  ]}
                >
                  <Text
                    numberOfLines={numberOfTitleLine}
                    bold={boldText}
                    type={titleType}
                    style={{ ...s.primaryTitle, ...textStyle }}
                  >
                    {title}
                  </Text>
                </View>
              )}
           
            </View>
          </View>
        </Animated.View>
      </Touch>
    );
  };

const s = StyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row-reverse',
  },
  primaryStyle: {
    flex: 1,
  },
  primaryIcon: {
    color: Color.light,
  },
  primaryTitle: {
    color: Color.light,
  },
  title: {
    flexDirection: 'column',
    width: '85%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    flexDirection: 'column',
    width: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

Button.propTypes = {
  type: PropTypes.oneOf(['primary', 'mini']),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  title: PropTypes.string,
  icon: PropTypes.string,
  mainColor: PropTypes.string,
  titleType: PropTypes.string,
  iconSize: PropTypes.number,
  onPress: PropTypes.func,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
};

Button.defaultProps = {
  style: {},
  type: 'primary',
  iconSize: 16,
  height: 45,
  width: 200,
  onPress: () => false,
  loading: false,
  disabled: false,
  titleType: 'heading4',
};

export { Button };
