/* eslint-disable consistent-return */
/* eslint-disable react/jsx-curly-newline */
/* eslint-disable react/no-array-index-key */
import React, { useState, useEffect, useRef } from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';
import PropTypes from 'prop-types';
import { DEVICE_HEIGHT, DEVICE_WIDTH } from '../../constants';
import { Condition } from './index';
import { getCurrentLocation, getMarkers } from '../../helpers/map';
import { Image } from 'react-native';

MapboxGL.setAccessToken('sk.eyJ1IjoiYW1pcmhvc3NlaW52dCIsImEiOiJjbDMwbDdyaXcwNnl3M2txcTM4Ymt1NGMzIn0.eiX9cuCXYuixq2v89n3ALA');


const Map =
  ({  
    style,
    showOrigins = true,
    showUserLocation = true,
    pickAddress = false,
    onRegionDidChange = () => { },
    addresses = [],
    zoomEnabled = true,
    scrollEnabled = true,
    pitchEnabled = true,
    rotateEnabled = true,
    dragPan = true,
    dragRotate = true,
    doubleClickZoom = true,
    autoFocus = true,
  }) => {
    const [current, setCurrent] = useState([51.389, 35.6892]);
    const cameraRef = useRef();


    const markers = getMarkers(addresses);



    const flyCamera = async location => {
      if (!cameraRef.current) return;
      const currentLocation = await getCurrentLocation();
      setCurrent(currentLocation);
      const setCameraConfig = {
        zoomLevel: 15,
        animationDuration: 500,
        animationMode: 'flyTo',
        centerCoordinate: location ? location : currentLocation,
      };
      cameraRef.current.setCamera(setCameraConfig);
    };

    const getAdrBoundary = () => {
      const minLng = Math.min(
        ...addresses.map(adr => {
          return adr?.lng;
        }),
      );
      const minLat = Math.min(
        ...addresses.map(adr => {
          return adr?.lat;
        }),
      );
      const maxLng = Math.max(
        ...addresses.map(adr => {
          return adr?.lng;
        }),
      );
      const maxLat = Math.max(
        ...addresses.map(adr => {
          return adr?.lat;
        }),
      );

      if (addresses.length) {
        return [
          [minLng, minLat],
          [maxLng, maxLat],
        ];
      } else return false;
    };

    const fitBound = async () => {
      const cameraPage = getAdrBoundary();
      console.log(cameraPage, 'camerapage');
      if (!cameraRef.current || !cameraPage) return;
      setTimeout(() => {
        cameraRef?.current?.setCamera({
          duration: 0.5,
          mode: MapboxGL?.CameraModes?.None,
          bounds: {
            ne: cameraPage[0],
            sw: cameraPage[1],
          },
        });
      }, 1000);
    };

    useEffect(() => {
      flyCamera();
    }, []);


    useEffect(() => {
      if (markers.length && autoFocus) { 
        setTimeout(() => {
          fitBound();
        }, 1000);
      }
    }, [markers]);



    const RenderMarkers = () => (
      <>
        {markers.map((cordinate, index) => (
          <>
            <MapboxGL.PointAnnotation
              selected
              key={index}
              id="id"
              onDragStart={() => console.log("pressed")}
              title="End Location"
              coordinate={[cordinate[0], cordinate[1]]}
              draggable
            >
              <Image
                source={{ uri: "https://toppng.com/uploads/preview/restaurant-png-11554005053riiacqdjki.png" }}
                style={{ height: 75, width: 75 }}
              />

            </MapboxGL.PointAnnotation>
          </>
        ))}
      </>
    );

    return (
      <MapboxGL.MapView

        compassEnabled={false}
        onRegionDidChange={e =>
          pickAddress && onRegionDidChange(e.geometry.coordinates)
        }
        zoomEnabled={zoomEnabled}
        scrollEnabled={scrollEnabled}
        pitchEnabled={pitchEnabled}
        rotateEnabled={rotateEnabled}
        dragPan={dragPan}
        dragRotate={dragRotate}
        doubleClickZoom={doubleClickZoom}
        zoomLevel={10}
        showUserLocation
        userTrackingMode={1}
        logoEnabled={false}
        centerCoordinate={current}
        attributionEnabled={false}
        style={style}
      >

        <Condition condition={showUserLocation}>
          <MapboxGL.UserLocation
            renderMode={'native'}
            visible
            animated
            showsUserHeadingIndicator
          />
        </Condition>
        <MapboxGL.Camera
          ref={cameraRef}
          zoomLevel={12}
          pitch={0}
          followPitch={0}
          centerCoordinate={current}
        />
        <Condition condition={showOrigins}>
          <RenderMarkers />
        </Condition>
      </MapboxGL.MapView>
    );
  };

Map.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

Map.defaultProps = {
  style: {
    flex: 1,
    height: DEVICE_HEIGHT,
    width: DEVICE_WIDTH,
  },
};

export { Map };
