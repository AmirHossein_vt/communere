/* eslint-disable react/require-default-props */
import React from 'react';
import {
  StyleSheet,
  Text as RNTEXT,
  View
} from 'react-native';
import PropTypes from 'prop-types';
import { Color } from '../../constants/index';
import { normalizeFont } from '../../helpers/common';

const Text = ({
  type,
  unitTextStyle,
  children,
  externalStyle,
  light,
  dark,
  align,
  price = false,
  unit = price ? '$' : '',
  bold = false,
  unitBold = false,
  numberOfLines = 10,
}) => (
  <View
    style={{
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <RNTEXT
      style={[
        { fontFamily: 'Shabnam' },
        style.span,
        style.mini,
        light && style.light,
        dark && style.dark,
        align && { textAlign: align },
        style.colorHandle,
        unitTextStyle,
        unitTextStyle?.fontSize && {
          fontSize: normalizeFont(unitTextStyle?.fontSize),
        },
        unitBold && { fontFamily: 'Shabnam-Bold' },
        !unitTextStyle && { fontSize: 12 },
      ]}
    >
      {' '}
      {unit}
    </RNTEXT>

    <RNTEXT
      style={[
        { fontFamily: 'Shabnam' },
        style.defaultText,
        style[type],
        light && style.light,
        dark && style.dark,
        align && { textAlign: align },
        externalStyle && externalStyle,
        externalStyle?.fontSize && {
          fontSize: normalizeFont(externalStyle?.fontSize),
        },
        bold && { fontFamily: 'Shabnam-Bold' },
      ]}
      numberOfLines={numberOfLines}
    >
      {children}
    </RNTEXT>

  </View>
);

    

const style = StyleSheet.create(
  {
  progressText: {
    fontSize: 16,
    color: Color.golden,
  },
  defaultText: {
    fontSize: normalizeFont(16),
    color: Color.secondary,
  },
  detailText: {
    color: Color.primary,
    fontSize: normalizeFont(14),
  },
  heading1: {
    color: Color.dark,
    fontSize: normalizeFont(22),
  },
  heading2: {
    color: Color.dark,
    fontSize: normalizeFont(20),
  },
  heading3: {
    color: Color.dark,
    fontSize: normalizeFont(18),
  },
  heading4: {
    color: Color.dark,
    fontSize: normalizeFont(16),
  },
  heading5: {
    color: Color.dark,
    fontSize: normalizeFont(14),
  },
  heading6: {
    color: Color.dark,
    fontSize: normalizeFont(12),
  },
  span: {
    color: Color.dark,
    fontSize: normalizeFont(13),
  },
  mini: {
    color: Color.gray,
    fontSize: normalizeFont(12),
  },
  paragraph1: {
    fontSize: normalizeFont(14),
  },
  paragraph2: {
    fontSize: normalizeFont(18),
  },
  light: {
    color: Color.light,
  },
  dark: {
    color: Color.light,
  },
  extraMini: {
    color: Color.gray,
    fontSize: normalizeFont(8),
  },
  dateExtraMini: {
    color: Color.gray,
    fontSize: normalizeFont(10),
  },
  colorHandle: {
    color: Color.darkModeGray,
  },
});



Text.propTypes = {
  type: PropTypes.oneOf([
    'heading1',
    'heading2',
    'heading3',
    'heading4',
    'heading5',
    'span',
    'paragraph1',
    'paragraph2',
    'mini',
  ]),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  align: PropTypes.oneOf(['right', 'center', 'left', 'justify']),
};



export { Text };
