import { Vibration } from 'react-native';
import { storageService } from './storageService';

class VibrationService {
  static isEnable = true;

  static tiny(time = 40) {
    this.isEnable && Vibration.vibrate(time);
  }

  static async stopVibrate() {
    Vibration.cancel();
  }

  static async changeVibrationStatus(value) {
    this.isEnable = value;
  }

  static patternVibrate(pattern) {
    this.isEnable && Vibration.vibrate(pattern, true);
  }

  static async loadInitialStatus() {
    return new Promise(resolve => {
      storageService
        .load('vibrationStatus')
        .then(resolve)
        .catch(() => {
          resolve(true);
        });
    });
  }
}

export default VibrationService;
