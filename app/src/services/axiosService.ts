import axios from 'axios';
import { API_URL } from '../constants/';
import { showToast } from './snackBarService';
import { dialogues } from '../constants';

const axiosService = axios.create({
  headers: {
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
  },
  baseURL: API_URL,
  timeout: 10000,
});
axiosService.interceptors.request.use(request => {
  return request;
});

axiosService.interceptors.response.use(
  response => {

    if (response.data?.object) {
      if (typeof response.data?.object === 'boolean') {
        return response.data;
      } else {
        return response.data?.object;
      }
    }
    return response.data;
  },
  async e => {
    const responsePayload =
      e && e.response ? e.response.data : e.message;
      console.log(JSON.stringify(e))

    const error = responsePayload?.errors ?? dialogues.noMessageError
    if (error) {
      showToast({
        text:error,
        mode: 'error',
        duration: 2000,
      });
    }

    return Promise.reject(error);
  },
);

export default axiosService;
