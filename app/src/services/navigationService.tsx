import React, { createRef } from 'react';
import { v4 as randomUIID } from 'uuid';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import {
    NavigationContainer,
    CommonActions,
} from '@react-navigation/native';
import { NAVIGATION } from '../constants/schema';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

enableScreens();

const Stack = createNativeStackNavigator();
export const isReadyRef = createRef();
export const navigationRef = createRef();
const Tab = createBottomTabNavigator();

export const RenderNavigationContainer = ({ children }) => {

    const tabBarScreens = NAVIGATION.stacks.filter(stack => !stack.hiddenInTabBar)
    const WideScreens = NAVIGATION.stacks.filter(stack => stack.hiddenInTabBar)

    return (
        <NavigationContainer
            ref={navigationRef}
            onReady={() => {
                isReadyRef.current = true;
            }}
        >
            <Stack.Navigator>

                {WideScreens.map(stack => {
                    return stack.screens.map(screen => (
                        <Stack.Screen
                            key={randomUIID}
                            name={screen.Name}
                            component={screen.Component}
                            options={{
                                headerShown: false
                            }}
                        />
                    ))
                })}

                <Stack.Screen name="Tab" component={() => (
                    <Tab.Navigator
                        screenOptions={{
                            headerShown: false,
                            tabBarActiveTintColor: '#e91e63',

                        }}
                    >
                        {tabBarScreens.map(stack => {
                            return stack.screens.map(screen => (
                                <Tab.Screen
                                    key={randomUIID}
                                    name={screen.Name}
                                    component={screen.Component}

                                />
                            ))
                        })}
                    </Tab.Navigator>
                )} />



            </Stack.Navigator>

            {children}
        </NavigationContainer>
    );
};

export const navigate = (name, params) => {
    if (isReadyRef.current && navigationRef.current) {
        if (name === 'goBack') {
            navigationRef.current.dispatch(CommonActions.goBack());
        } else {
            navigationRef.current.navigate(name, params);
        }
    } else {
        console.log("can not navigate")
    }
};


