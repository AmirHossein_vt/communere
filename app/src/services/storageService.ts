import AsyncStorage from '@react-native-community/async-storage';

class Storage {
  save(key, data) {
    const items =
      typeof data === 'string' ? data : JSON.stringify(data);
    AsyncStorage.setItem(key, items);
  }

  remove(key) {
    AsyncStorage.removeItem(key);
  }

  clear() {
    AsyncStorage.clear();
  }

  load(key, isString = false) {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem(key)
        .then((data) => {
          if (data) {
            resolve(isString ? JSON.parse(data) : data);
          } else {
            reject();
          }
        })
        .catch(reject);
    });
  }
}

const storageService = new Storage();

export { storageService };
