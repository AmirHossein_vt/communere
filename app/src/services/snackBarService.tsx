import React from 'react';
import Toast from 'react-native-toast-message';

let showToast = object => [object];

const SnackBarService = () => {

  showToast = ({
    text,
    mode = 'success',
    actions = [],
    duration = 2000,
    didFinished = () => {},
  }) => {
    Toast?.show({
      type: mode,
      text1: text,
      props: { actions, mode },
      visibilityTime: duration,
      onHide: didFinished,
    });
  };


  return (
    <Toast ref={ref => Toast.setRef(ref)} />
  );
};

export { SnackBarService, showToast };
