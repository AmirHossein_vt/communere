import { DashboardScreen,SplashScreen,LoginScreen } from "../containers/index";

export const NAVIGATION = {
    stacks:[
        {
            name: 'splash',
            hiddenInTabBar: true,
            screens: [
              {
                Name: 'SplashScreen',
                Component: SplashScreen,
                
              },
            ],
          },
          {
            name: 'login',
            hiddenInTabBar: true,
            screens: [
              {
                Name: 'LoginScreen',
                Component: LoginScreen,
                
              },
            ],
          },
          {
            name: 'app',
            screens: [
              {
                Name: 'DashboardScreen',
                Component: DashboardScreen,
              },
            ],
          },
    ]
};