import { LOGOUT_USER, NEW_USER } from "./types";

export const initialState = {
    token: null
};


export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case NEW_USER:
            return { ...action.payload };
        case LOGOUT_USER:
            return { initialState };
        default:
            return state;
    }
}