import {userReducer} from './user/resucers';
import { applyMiddleware, createStore, combineReducers} from 'redux';

const rootReducer = combineReducers({
 user: userReducer
});

export default createStore(rootReducer);