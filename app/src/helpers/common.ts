import { PixelRatio } from "react-native";
import { DEVICE_WIDTH } from "../constants";

export const normalizeFont = size => {
    const scale = DEVICE_WIDTH / 330;
  
    const newSize = size * scale;
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  };