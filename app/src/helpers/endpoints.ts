import axiosService from '../services/axiosService';

const loginRequest = async ({
  email,
  password,
}) => {
  return axiosService({
    method: 'POSt',
    url: `user/login`,
    data: {
     user:{
      email,
      password
     }
    },
  });
};

export {
  loginRequest
  
};
