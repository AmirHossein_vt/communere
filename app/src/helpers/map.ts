
import Geolocation from '@react-native-community/geolocation';
import { Logger } from '@react-native-mapbox-gl/maps';


export const getCurrentLocation = () =>
  new Promise(resolve => {
    Geolocation.getCurrentPosition(
      position => {
        const currentLongitude = JSON.stringify(
          position.coords.longitude,
        );
        const currentLatitude = JSON.stringify(
          position.coords.latitude,
        );
        resolve([currentLongitude, currentLatitude]);
      },
      error => {
       console.log(error);
      },
      {
        enableHighAccuracy: false,
        timeout: 20000,
      },
    );
  });

export const customizeMapBoxLogger = () => {
  Logger.setLogCallback(log => {
    const { message } = log;
    if (
      message.match(
        'Request failed due to a permanent error: Canceled',
      ) ||
      message.match(
        'Request failed due to a permanent error: Socket Closed',
      )
    ) {
      return true;
    }
    return false;
  });
};


export const getMarkers = (addresses = []) => {
  const finalData = [];
  addresses.map(({ lng, lat }) => {
    finalData.push([lng, lat]);
  });
  return finalData;
};