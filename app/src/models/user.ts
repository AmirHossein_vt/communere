import { navigate } from '../services/navigationService';
import { storageService } from '../services/storageService';
import { LOGOUT_USER, NEW_USER } from '../stores/user/types';

class UserModel {
  static userInfo = {};

  static async setter(user, dispatch) {
    this.userInfo = user;
    await storageService.save('@USER', user);
    dispatch({
      type: NEW_USER,
      payload: user
    })
  }

  static async logout(dispatch) {
    this.userInfo = {};
    dispatch({ 
      type: LOGOUT_USER,
    })
  }

  static async load(dispatch) {

    return new Promise((resolve , reject) => {
      storageService.load('@USER').then(freshUser => {
        this.userInfo = JSON.parse(freshUser);
        dispatch({
          type: NEW_USER,
          payload: JSON.parse(freshUser)
        })
        resolve(JSON.parse(freshUser));
      }).catch(() => {
          const user = {
          token: null
        }
        this.userInfo = user;
        dispatch({
          type: NEW_USER,
          payload: user
        })
        navigate("LoginScreen")
        reject()
      })
    })
    
  }

  static getter(param) {
    return this.userInfo?.[param] !== undefined ||
      this.userInfo?.[param] !== null
      ? this.userInfo?.[param]
      : '';
  }


}

export { UserModel };
