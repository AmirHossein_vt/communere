

![Logo](https://media-exp1.licdn.com/dms/image/C4E1BAQHekiyuoDsWZQ/company-background_10000/0/1641914248422?e=2147483647&v=beta&t=6XJEbo7yjxojEZSfLeC6bBwYlQZsFlN3k7bFlkJEEts)


# Booking

A test service for booking resaurants tables

# Description

#### development time : 26H


#### a video of the application to avoid the hassle of running the project to view the code output:

[![Watch the video](https://www.maketecheasier.com/assets/uploads/2018/06/best-code-editor-apps-mac-vim.jpg)](https://uupload.ir/view/screenrecording_ia94.mov/)

# 

Due to time constraints for me
Not all sections mentioned in the task description are done
And in some cases much more than what is required and in some cases less than what is required developed.
But in general, all the necessary parts that have been implemented to determine the programmer's ability and handling challenges skill .
And the rest are very simple , just some simple crud operation , tables , ... in client side



In addition to the list of items that can be improved in Production mode :
 
- using mem cache/redis for handling cache in nodeJs
- configure progaurd in react native
- configure hermes engine in react native
- More security measures  
- monitoring tools like pm2 for nodeJs
- huskey/eslint/prretier
- gzip
- socket/SSR for realtime tables avaiablity for booking
- better UI/UX :)
- ...
# Server Deployment


#### Install all dependencies
		npm install

#### Starting the API in stagging mode
If mongodb service is not running

		sudo service mongod start

Start the API

    node app.js

#### Starting in production mode
    
    NODE_ENV=production JWT_SECRET=<secret code for JWT> MONGO_URL=<url for mongoDB> node index.js

#### Configuration
Modify values in config/index.js for custom configuration.


# Appliication Deployment


running android command  :

```bash
  npm run android
```




# API Reference


#### /user
-	POST - create customer account
	-	Permission - self
	- required data: name, phone, email
	- optional data: password
-	GET - get customer data.
	-	Permission - self
	-	required data: Authentication token
#### /user/login
- POST - validate customer login and return jwt token
	-	Permission - self
	-	required data: email, password
#### /user/restaurant/login
-	POST - validate RestaurantOwner login and return jwt token
	-	Permission - self
	-	required data: email, password
#### /restaurant
-	POST - Create new restaurant by RestaurantOwner
	-	required data: name, address, business hours
	-	optional data: description
-	GET - Get restaurants owned by the RestaurantOwner
	-	required data: Authentication token


#### /restaurant/bookings/:restaurantId
-	GET - bookings of a restaturant
	-	required data: Authentication token


#### /restaurant/booking/:restaurantId/:bookingId
-	GET - get booking of a restaurant by bookingId
	-	required data: Authentication token
-	PUT - update booking of a restaurant by bookingId
	-	required data: Authentication token, noOfPersons, bookingFrom
	- optional data: table (type: objectId)

#### /restaurant/booking/:restaurantId/:bookingId/status
-	PUT - Change bookingStatus
	-	Confirm / Cancel a booking
	-	required data: Authentication token, bookingStatus

#### /restaurants
-	GET - list all verfied restaurants for public view
	-	permission: public

#### /restaurant/table/:restaurantId
-	POST - Add table(s) to their restaurant by RestaurantOwner
	-	required data: Authentication token, tables, tableIdentifier, capacity
	-	optional data: description
#### /restaurant/table/:restaurantId/:tableId
-	GET - Read table details
	-	required data: Authorization token

#### /restaurant/tables/:restaurantId
-	GET - List tables for particular restaurant
	-	required data: Authorization token

#### /booking
-	POST - Create booking in case its available
	-	Required data: token, restaurant, noOfPersons, bookingFrom
	- Permision: login customer
-	Get - Get all the bookings by user Id
	-	required data: Authentication token
	-	permission Customer
#### /booking/:bookinId
-	permission- customer, self
	-	required data: Authentication token


## Authors

- [@AmirHossein_vt](https://gitlab.com/AmirHossein_vt)




## 🔗 Links
[![resume](https://img.shields.io/badge/my_resume-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://docs.google.com/document/d/17YSIJf9M0I5RyG06ErVy68crDttBn15b19mJ0wlRH5A/edit?usp=sharing)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/amirhossein-vatani-6a7a13134)
[![instagram](https://img.shields.io/badge/instagram-red?style=for-the-badge&logo=instagram&logoColor=white)](https://instagram.com/amir_hossein_vatani)

